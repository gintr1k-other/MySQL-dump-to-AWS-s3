# Bash script to dump databases and upload it to ASW S3
---

## English
### Description
This is a Bash script which dump all mysql databases and uploading to ASW S3.

### Installation
#### Step 1
Get API keys of your S3 and configure `s3cmd` tool ([tutorial](https://www.digitalocean.com/community/tutorials/how-to-configure-s3cmd-2-x-to-manage-digitalocean-spaces)).

#### Step 2
You need to clone this git repo.
```
git clone https://gitlab.com/gintr1k-other/MySQL-dump-to-AWS-s3.git
```

#### Step 3
Add to your MySQL database a backup user for script.

#### Step 4
Edit your crontab and add 3 tasks.
```
*/1 * * * * /bin/sh -c 'cd /path-to-repo/MySQL-dump-to-AWS-s3 && /usr/bin/git pull origin master'
0 * * * * /bin/bash /path-to-repo/MySQL-dump-to-AWS-s3/backup.sh Server001 Space YOUR_BACKUP_USER YOU_BACKUP_PASSWORD
* * */1 * * /bin/bash /path-to-repo/MySQL-dump-to-AWS-s3/remove_old_backups.sh Server001 Space
```

#### Step 5
Enjoy!

---

## Русский
### Описание
Это Bash-скрипт для дампа всей БД MySQL и загрузки в "облако" AWS S3.

### Установка
#### Шаг 1
Получите API ключи от AWS S3 и настроите утилиту `s3cmd` ([howto](https://www.digitalocean.com/community/tutorials/how-to-configure-s3cmd-2-x-to-manage-digitalocean-spaces)).

#### Шаг 2
Необходимо клонировать этот репозиторий.
```
git clone https://gitlab.com/gintr1k-other/MySQL-dump-to-AWS-s3.git
```

#### Шаг 3
Добавьте в MySQL БД пользователя, который будет и производить все бэкапы.

#### Шаг 4
Добавьте в crontab 3 задачи.
```
*/1 * * * * /bin/sh -c 'cd /path-to-repo/MySQL-dump-to-AWS-s3 && /usr/bin/git pull origin master'
0 * * * * /bin/bash /path-to-repo/MySQL-dump-to-AWS-s3/backup.sh Server001 Space YOUR_BACKUP_USER YOU_BACKUP_PASSWORD
* * */1 * * /bin/bash /path-to-repo/MySQL-dump-to-AWS-s3/remove_old_backups.sh Server001 Space
```

#### Шаг 5
Наслаждайтесь!

---
